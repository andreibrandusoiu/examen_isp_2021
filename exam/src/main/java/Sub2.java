import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Sub2 {
    public static void main(String[] args) {
        new Win();
    }
}

class Win extends JFrame implements ActionListener {
    private JTextArea tA;
    private JTextField tF;
    private JButton b;

    Win() {
        setTitle("Sub2");
                setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(null);
        setSize(400, 400);
        tF = new JTextField();
        tF.setBounds(30, 20, 80, 20);
        tA = new JTextArea();
        tA.setBounds(130, 20, 120, 40);
        b = new JButton("Show!");
        b.setBounds(30, 70, 70, 30);
        b.addActionListener(this);
        add(b);
        add(tF);
        add(tA);
        setVisible(true);


    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            BufferedReader in = new BufferedReader(new FileReader(tF.getText()));
            if (e.getSource().equals(b)) {
                String s;

                while ((s = in.readLine()) != null) {
                    tA.append(s + "\n");
                }

            }
        } catch (IOException fileNotFoundException) {
            fileNotFoundException.printStackTrace();


        }
    }
}