public class Sub1 {
    public static void main(String[] args) {
       I i1 = new I();
       i1.i(new J("New"));
    }
}

interface U {
    void f();
}

class I implements U {
    private long t;
    private K k;

    public void f() {
        t=t*2;
    }

    public void i(J j) {
        System.out.println(j);
    }
}

class J {
    private String message;

    J(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "J{" +
                "message='" + message + '\'' +
                '}';
    }
}

class N {
    private I i;

}

class K {
    private L l;
    private S s;

    K(S s) {
        this.l = new L();
        this.s = s;
    }
}

class L {
    public void metA() {
        System.out.println("method A");

    }
}

class S {
    public void metB() {
        System.out.println("method B");

    }
}